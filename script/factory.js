class Media {
    constructor(media){
        this.title = media.title;
        this.id = media.id;
        this.photographerId = media.photographerId;
        this.tags = media.tags;
        this.likes = media.likes;
        this.date = media.date;
        this.price = media.price;
    }

    createElement(index){
        let gallery = document.querySelector(".gallery");
        // Elements creation
        let mediaCard = document.createElement('article');
        let thumbnail = document.createElement('button');
        let description = document.createElement('div');
        let title = document.createElement('p');
        let numbers = document.createElement('div');
        let price = document.createElement('p');
        let like = document.createElement('p');
        let heart = document.createElement('button');

        // Class
        mediaCard.classList.add('media');
        mediaCard.id = index;
        thumbnail.classList.add('media__thumb');
        description.classList.add('media__description');
        title.classList.add('media__description__title');
        numbers.classList.add('media__description__numbers');
        heart.classList.add('media__description__numbers__heart');

        // Contents
        title.innerHTML = this.title;
        price.innerHTML = this.price + "€";
        heart.innerHTML = '<i class="fas fa-heart"></i>';
        // set up Image
        thumbnail.style.backgroundImage = "url('" + this.bgSrc + "')";

        // Attributes
        thumbnail.setAttribute("aria-label", "open carousel");
        title.setAttribute("tabindex", "0");
        like.setAttribute("tabindex", "0");
        price.setAttribute("tabindex", "0");
        heart.setAttribute("aria-label", "likes");

        // Like counter
        let likeCount = this.likes;
        like.innerHTML = likeCount;
        heart.addEventListener('click', function(){
            likeCount++;
            like.innerHTML = likeCount;
        });

        // Organization
        gallery.appendChild(galleryContent);
        galleryContent.appendChild(mediaCard);
        mediaCard.appendChild(thumbnail);
        mediaCard.appendChild(description);
        description.appendChild(title);
        description.appendChild(numbers);
        numbers.appendChild(price);
        numbers.appendChild(like);
        numbers.appendChild(heart);
    }
}

class Image extends Media {
    constructor(media){ 
        super(media);
        this.mediaSrc = "../ressources/photo/" + media.photographerId + "/" + media.image;
        this.bgSrc = "../ressources/photo/" + media.photographerId + "/" + media.image;
        this.type = "image";
    }
}

class Video extends Media {
    constructor(media){ 
        super(media);
        this.mediaSrc = "../ressources/photo/" + media.photographerId + "/" + media.video;
        this.bgSrc = "../ressources/photo/videoThumb/" + media.id + ".jpg"
        this.type = "video";
    }
}

function getMedias(medias){

    const selProfilMedias = [];

    medias.forEach((media) => {
        if(media.image === undefined){
            selProfilMedias.push(new Video(media));
        }else{
            selProfilMedias.push(new Image(media));
        }
    });
    console.log(selProfilMedias);
    return selProfilMedias;
}