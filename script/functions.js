// INDEX FUNCTIONS
// Photographers thumbnails creation for index.html
const photographersThumbnailsCreation = (photographers) => {
    let photographersSection = document.getElementById('photographers');

    for(let i = 0; i < photographers.length; i++){
        //elements creation
        let photographer = document.createElement('a');
        let identity = document.createElement('header');
        let user = document.createElement('div');
        let info = document.createElement('section');
        let tagList = document.createElement('ul');
        let portrait = document.createElement('img');
        let name = document.createElement('h2');
        let location = document.createElement('p');
        let tagline = document.createElement('p');
        let price = document.createElement('p');
    
        //Tags elements creation
        let tags = photographers[i].tags;
        for(let j = 0; j < tags.length; j++){
            let tag = document.createElement('li');
            tag.innerHTML = '#' + tags[j];
            tagList.appendChild(tag);
            tag.classList.add('taglist__tag');
        }
    
        //Class adding
        photographer.classList.add('photographer');
        photographer.id = i;                                                // Création d'un id unique pour chaque profil
        identity.classList.add('photographer__identity');
        user.classList.add('user');
        portrait.classList.add('user__portrait');
        portrait.classList.add('user__portrait--' + photographers[i].id);
        name.classList.add('name');
        info.classList.add('photographer__info');
        location.classList.add('location');
        tagline.classList.add('photographer__info__tagline');
        price.classList.add('photographer__info__price');
        tagList.classList.add('taglist');
    
        //URL for each profiles
        let urlParams = "./html_pages/profils.html";
        urlParams = urlParams + "?id=" + photographers[i].id
    
        //Contents 
        photographer.setAttribute('href', urlParams);
        portrait.setAttribute('src', "./ressources/photo/photographersIdPhotos/" + photographers[i].id + ".jpg");
        portrait.setAttribute('role', "img");
        portrait.setAttribute('alt', ("profil de " + photographers[i].name));
        name.innerHTML = photographers[i].name;
        location.innerHTML = photographers[i].city + ', ' + photographers[i].country;
        tagline.innerHTML = photographers[i].tagline;
        price.innerHTML = photographers[i].price + '€/jour';
    
        //Organization
        photographersSection.appendChild(photographer);
        photographer.appendChild(identity);
        photographer.appendChild(info);
        identity.appendChild(user);
        user.appendChild(portrait);
        identity.appendChild(name);
        info.appendChild(location);
        info.appendChild(tagline);
        info.appendChild(price);
        info.appendChild(tagList);
    }
}









// TAGS NAVIGATION MANAGER
const tagsNavigationManager = (array) =>{

    // Hide or make visible profils
    const findTag = (currentTags) => {
        for(let j = 0; j < array.length; j++){                                // Pour chaque élément
            let elt = document.getElementById(j)                            // On relie l'élement html à l'élément javascript via un id unique par élément
            elt.classList.add('hidden')                                     // Ajout d'une classe .hidden qui cache l'élément
            for(let k = 0; k < array[j].tags.length; k++){                    // Pour chaque tag de chaque photographe
                for(let m = 0; m < currentTags.length; m++){                  // Pour chaque tag séléctionné par l'utilisateur
                    if(array[j].tags[k] === currentTags[m]){                  // Si il y a un tag correspondant au tag séléctionné
                        elt.classList.remove('hidden');                     // Suprression de la classe .hidden
                        break;
                    }
                }
            }
        }
    }

    // Remove Tag from currentTags[]
    const removeTag = (btnId, n) => {
        for(let p = 0; p < currentTags.length; p++){                            // Pour chaque tag de currentTags[]
            if(currentTags[p] == btnId){                                        // Si le tag est le même que le tag séléctionné par l'utilisateur
                currentTags.splice(p, 1);                                       // On le prend dans le tableau et on le retire
                break;
            }
        }
        tagButtonLabels[n].classList.remove('isChecked');
    }

    // Add or remove Tags from currentTags[]
    const manageTags = (btnId, n) => {
        if(tagButtons[n].checked == true){
            currentTags.push(btnId);
            findTag(currentTags);
            tagButtonLabels[n].classList.add('isChecked');
        }else{
            removeTag(btnId, n);
            findTag(currentTags);
        }
        if(currentTags.length == 0){                                            // Si aucun tag n'est séléctionné, on affiche tous les éléments
            findTag(['portrait', 'art', 'fashion', 'architecture', 'travel', 'sport', 'animals', 'events']);
        }
    }

    // Tags navigation
    let currentTags = [];                                                       
    const tagButtons = document.getElementsByClassName('tag-btn');
    const tagButtonLabels = document.getElementsByClassName('btn');
    // Tags Events manager
    for(let n = 0; n < tagButtons.length; n++){
        let btnId = tagButtons[n].id;
        tagButtons[n].addEventListener('change', function(){
            manageTags(btnId, n);
        });
    }

}








// PROFILS FUNCTIONS

// Look for the good profil in data[].photographers[] 
const profilFinder = (data, id) => {
    let profil;
    for(let i = 0; i < data[0].photographers.length; i++){
        if(data[0].photographers[i].id == id){
            profil = data[0].photographers[i];
            break;
        }
    }
    return profil;
}

// search all photograph's medias
const mediaFinder = (data, id) => {
    let mediaArray = [];
    for(let i = 0; i < data[0].media.length; i++){
        if(data[0].media[i].photographerId == id){
            mediaArray.push(data[0].media[i]);
        }
    }
    return mediaArray;
}

// Header elements creation
const headerEltCreation = (selProfil) => {
    //Header DOM elements
    let name = document.querySelector('.name');
    let location = document.querySelector('.location');
    let tagline = document.querySelector('.presentation__info__tagline');
    let tagList = document.querySelector('.taglist');
    let contact = document.querySelector('.contact-btn');
    let portrait = document.querySelector('.portrait');
    let like = document.querySelector('.like');
    let price = document.querySelector('.price');
    let likesBtn = document.getElementById('profil-likes-btn');

    //Tags elements creation
    let tags = selProfil.tags;
    for(let j = 0; j < tags.length; j++){
        let tagInput = document.createElement('input');
        let tag = document.createElement('label');
        tag.innerHTML = '#' + tags[j];
        tagList.appendChild(tagInput);
        tagList.appendChild(tag);
        tag.classList.add('taglist__tag', 'btn');
        tag.setAttribute('for', tags[j]);
        tagInput.setAttribute('type', 'checkbox');
        tagInput.setAttribute('role', 'checkbox');
        tagInput.setAttribute('aria-label', tags[j]);
        tagInput.id = tags[j];
        tagInput.classList.add('tag-btn');
    }

    //Contents
    name.innerHTML = selProfil.name;
    location.innerHTML = selProfil.city + ', ' + selProfil.country;
    tagline.innerHTML = selProfil.tagline;
    contact.innerHTML = "Contactez-moi";
    price.innerHTML = selProfil.price + '€/jour';

    // Like counter
    let likeCount = selProfil.like;
    like.innerHTML = likeCount;
    likesBtn.addEventListener('click', function(){
        likeCount++;
        like.innerHTML = likeCount;
    });

    //Attributes
    portrait.setAttribute('src', "../ressources/photo/photographersIdPhotos/" + selProfil.id + ".jpg");
    portrait.classList.add('user__portrait');
    portrait.classList.add('user__portrait--' + selProfil.id);
    portrait.setAttribute('alt', selProfil.name);
}

// SORTING GALLERY / Compare function
const compareValue = (key, order='desc') =>{ // asc = croissant / desc = decroissant
    
    if(key === "title"){
        order = "asc";
    }

    return function(a, b){

        const varA = (typeof a[key] === 'string') ? a[key].toUpperCase() : a[key]; // ? = conditional operator
        const varB = (typeof b[key] === 'string') ? b[key].toUpperCase() : b[key];
        
        let comparison = 0;

        if (varA > varB) {
            comparison = 1;
        } else if (varA < varB) {
            comparison = -1;
        }

        return (
            (order == 'desc') ? (comparison * -1) : comparison // change the array direction if order = desc
        );
    }

}