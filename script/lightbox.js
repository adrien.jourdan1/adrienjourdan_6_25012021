const lightboxManager = (galleryElts) => {
    // DOM Elements
    const lightbox = document.querySelector(".lightbox");
    const mediaThumbs = document.getElementsByClassName("media__thumb");
    const closeBtnLightbox = document.querySelector(".close-lightbox");
    const displayedImg = document.querySelector(".displayed-img");
    const displayedVid = document.querySelector(".displayed-video");
    const previousBtn = document.querySelector(".previous");
    const nextBtn = document.querySelector(".next");
    let currentIndex = 0;
    
    // open lightbox event
    for(let u = 0; u < mediaThumbs.length; u++){
        mediaThumbs[u].addEventListener("click", function(){
            lightbox.style.display = "flex";
            document.body.setAttribute('aria-hidden', 'true');
            lightbox.setAttribute('aria-hidden', 'false');
            displayMedia(u);
            currentIndex = u;
        });
    }

    // close lightbox event
    closeBtnLightbox.addEventListener('click', function(){
        lightbox.style.display = "none";
    });
    document.addEventListener('keydown', function(e){
        if(e.key == "Escape"){
            lightbox.style.display = "none";
        }
    });   

    // previous media event
    previousBtn.addEventListener('click', function(){
        displayMedia(currentIndex-1);
        currentIndex--;
    });
    document.addEventListener('keydown', function(e){
        if(e.key == "ArrowLeft" && galleryElts[currentIndex-1] !== undefined){
            displayMedia(currentIndex-1);
            currentIndex--;
        }
    });   

    // next media event
    nextBtn.addEventListener('click', function(){
        displayMedia(currentIndex+1);
        currentIndex++;
    });
    document.addEventListener('keydown', function(e){
        if(e.key == "ArrowRight" && galleryElts[currentIndex+1] !== undefined){
            displayMedia(currentIndex+1);
            currentIndex++;
        }
    });  

    // Display media function 
    const displayMedia = (i) => {

        // Init display block on buttons
        previousBtn.style.display = "block";
        nextBtn.style.display = "block";

        // Remove previous or next buttons if there is no media
        if(galleryElts[i-1] === undefined){
            previousBtn.style.display = "none";
        }
        if(galleryElts[i+1] === undefined){
            nextBtn.style.display = "none";
        }

        // Is media an image or video ?
        if (galleryElts[i].type === "video") {
            let mediaContent = galleryElts[i].mediaSrc;
            displayedVid.innerHTML = "";
            displayedVid.innerHTML = "<source src='" + mediaContent + "' type='video/mp4'>";
            displayedImg.style.display = 'none';
            displayedVid.style.display = 'block';
            displayedVid.setAttribute("controls", "");
            displayedVid.setAttribute('alt', galleryElts[i].mediaTitle);
            displayedVid.focus();
        }else{
            displayedImg.setAttribute('src', "");
            let mediaContent = galleryElts[i].mediaSrc;
            displayedImg.setAttribute('src', mediaContent);
            displayedVid.style.display = 'none';
            displayedImg.style.display = 'block';
            displayedImg.setAttribute('alt', galleryElts[i].mediaTitle);
            displayedImg.focus();
        }
    }
}