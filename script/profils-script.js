// ID url parameter retrieval / CF https://developer.mozilla.org/fr/docs/Web/API/URL/searchParams
let params = (new URL(document.location)).searchParams;
let id = parseInt(params.get('id'));

// Search the good Profil and associated medias
let selProfil = profilFinder(data, id);
let medias = mediaFinder(data, id);

// Document title set up
document.title = "Fisheye | " + selProfil.name;

// Header elements creation
headerEltCreation(selProfil);
modalManager(selProfil);

// Create a new array by factory method that contains all selected profil's medias 
let galleryElts = getMedias(medias); 

// Tags navigation
tagsNavigationManager(galleryElts);

// DOM element for sorting gallery
const gallerySorting = document.getElementById('gallery-sorting');
const galleryContent = document.getElementById('gallery-content');


// Initial gallery elements creation sort by popularity
galleryElts.sort(compareValue("likes", 'desc'));

galleryElts.forEach((media, index) => {

    media.createElement(index);
    lightboxManager(galleryElts);

});


// Change gallery sorting Event
gallerySorting.addEventListener('change', function(){

    galleryContent.innerHTML = "";
    galleryElts.sort(compareValue(gallerySorting.value, 'desc'));

    galleryElts.forEach((media, index) => {
        media.createElement(index);
        lightboxManager(galleryElts);
    });

});